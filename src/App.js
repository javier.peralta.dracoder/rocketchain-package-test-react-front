import './App.css';
import RocketchainPackage from 'rocketchain-package'
import {useState} from "react";
import {useForm} from "react-hook-form";

function App() {

    const [createdToken, setCreatedToken] = useState(null);
    const [walletAddress, setWalletAddress] = useState(null);
    const {register, handleSubmit} = useForm();

    RocketchainPackage.setClientServerUrl('http://rocketchain-server.test/api');

    const metamaskConnect = async () => {
        const connectedWalletAddress = await RocketchainPackage.metamaskConnect();

        setWalletAddress(connectedWalletAddress);
    }

    const tokenCreation = async () => {
        const createdToken = await RocketchainPackage.tokenCreate({
            nameOfToken: "Mi token creada por npm package",
            numberOfToken: 15000,
            symbolOfToken: "USDT",
            numberOfDecimals: 18,
            userWalletAddress: "0x00000", //TODO: tendría sentido que enviemos la wallet del usuario en la creacion de un token?
        });
console.log(createdToken)
        setCreatedToken(createdToken);
    }

    const onSubmit = data => {
        const tokenTransferred = RocketchainPackage.tokenTransfer(data.walletAddressToTransferTo, data.tokenAddress, data.amount);
    };

    return (
        <div className="App container">
            <h1 className={'py-2'}>ROCKETCHAIN DEV TEAM AWESOME</h1>
            <div className={'d-flex justify-content-around mb-2'}>
                <div className={'bg-secondary text-white p-3'}>
                    <p><b>Arturo's wallet</b></p>
                    <small>0x212f260404A05E954BC9285BFAe89D6B2bB45134</small>
                </div>

                <div className={'bg-secondary text-white p-3'}>
                    <p><b>Pablos's wallet</b></p>
                    <small>0x212f260404A05E954BC9285BFAe89D6B2bB45134</small>
                </div>

                <div className={'bg-secondary text-white p-3'}>
                    <p><b>Javier's wallet</b></p>
                    <small>0xe4513a7BCd98dE50Fe7984C82dAf5611beBa7170</small>
                </div>
            </div>
            <button
                className={'btn btn-success'}
                onClick={metamaskConnect}
            >
                Conectarse a metamask
            </button>
            <p className={'pt-5'}>
                <b>{walletAddress ? walletAddress : 'No se encuentra conectado a ninguna wallet.'}</b>
            </p>
            <br/>
            <div className={'d-flex justify-content-between'}>
                <div className={'col-6 px-2'}>
                    <button
                        className={'btn btn-primary mb-3'}
                        onClick={tokenCreation}
                    >
                        Crear token
                    </button>
                    <br/>
                    <hr/>
                    <code>
                        {createdToken && JSON.stringify(createdToken)}
                    </code>
                </div>
                <div className={'col-6 px-2'}>
                    <h3 className={'pb-4'}>Transferir tokens</h3>
                    <hr/>
                    <form
                        onSubmit={handleSubmit(onSubmit)}
                    >
                        <input
                            type={'text'}
                            placeholder={'walletAddressToTransferTo'}
                            className={'form-control mb-2'}
                            {...register("walletAddressToTransferTo")}
                        />

                        <input
                            type={'text'}
                            placeholder={'tokenAddress'}
                            className={'form-control mb-2'}
                            {...register("tokenAddress")}
                        />

                        <input
                            type={'text'}
                            placeholder={'amount'}
                            className={'form-control mb-2'}
                            {...register("amount")}
                        />

                        <button
                            className={'btn btn-warning btn-block w-100'}
                        >
                            Transferir
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default App;
